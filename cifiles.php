<?php

define('FILE_STATUS_TMP',0);
define('FILE_STATUS_OK',1);
define('FILE_STATUS_DELETED',2);
define('FILE_STATUS_HIDDEN',3);

define('FILE_SOURCE_UPLOAD' , 1 );
define('FILE_SOURCE_INK' , 2 );

class ciFiles
{
  protected $ci;
  protected $db;
  protected $_file_table = 'files';
  protected $_folder_table = 'folders';

  final public function __construct()
  {
    $this->store = (object)array();
    $this->ci =& get_instance();
    $this->db =& $this->ci->db;
  }

  /*
   * Return a single file object
   */
  public function getFile( $id )
  {
    $this->db->select('files.*');
    $this->db->from('files');
    $this->db->join('folders','folders.id = files.parent_id', 'left');
    $this->db->where(array('files.id' => (int)$id ));

    $file = $this->db->get()->row();

    $this->_nicePropsFile( $file );
    return $file;
  }

  /*
   * Return several file objects
   */
  public function getFiles( $ids )
  {
    $this->db->select('files.*,folders.client_id');
    $this->db->from('files');
    $this->db->join('folders','folders.id = files.parent_id', 'left');
    $this->db->where_in('files.id' , $ids );
    $this->db->where(array('files.status !=' => FILE_STATUS_DELETED ));

    $query = $this->db->get();

    if( !$query->num_rows() )
      return array();

    $files = array();
    foreach( $query->result() as $row )
    {
      $this->_nicePropsFile( $row );
      $files[ $row->id ] = $row;
    }

    return $files;
  }

  /*
   * Return the immediate children of a folder
   */
  public function getFolderChildren( $folder_id , $opts = array() )
  {
    $default_opts = array(
      'folders' => true,
      'files' => true,
      'exclude_deleted' => true,
      'exclude_hidden' => false
    );

    $opts = (object)array_merge( $default_opts , $opts );

    $children = (object) array('folders'=>array(),'files'=>array(),'count'=>0);

    $where_clause = array('parent_id' => (int)$folder_id );

    if( $opts->exclude_deleted && $opts->exclude_hidden )
      $where_clause[ 'status' ] = FILE_STATUS_OK;
    elseif( $opts->exclude_deleted )
      $where_clause[ 'status !=' ] = FILE_STATUS_DELETED;
    elseif( $opts->exclude_hidden )
      $where_clause[ 'status !=' ] = FILE_STATUS_HIDDEN;

    // folders
    if( $opts->folders )
    {
      $this->db->order_by('folder_name ASC');
      $query = $this->db->get_where( $this->_folder_table , $where_clause );

      if( $query->num_rows() > 0 )
      {
        foreach( $query->result() as $row )
        {
          $this->_nicePropsFolder( $row );
          $children->folders[ $row->id ] = $row;
        }
      }
    }

    // files
    if( $opts->files )
    {
      $this->db->order_by('filename ASC');
      $query = $this->db->get_where( $this->_file_table , $where_clause );

      if( $query->num_rows() > 0 )
      {
        foreach( $query->result() as $row )
        {
          $this->_nicePropsFile( $row );
          $children->files[ $row->id ] = $row;
        }
      }
    }

    $children->count = count( $children->folders ) + count( $children->files );

    return $children;
  }


  /*
   * Pull the user's root-most folder, optionally creating one as needed
   */
  public function getUserRootFolder( $user_id )
  {
    $query = $this->db->get_where( $this->_folder_table , array('parent_id'=>0,'user_id'=> (int)$user_id ) );

    // no user root folder? make one on the fly
    if( $query->num_rows() == 0 )
    {
      // can't use $this->addFolder here because of parent_id error checking, the root folder is
      // special because its the only folder that has parent_id  zero
      $insert_data = array(
        'user_id'=> $user_id ,
        'folder_name' => null ,
        'parent_id' => 0 ,
        'status' => FILE_STATUS_OK,
        'created' => gmdate('Y-m-d H:i:s')
      );

      $this->db->insert( $this->_folder_table , $insert_data );
      $folder_id = $this->db->insert_id();
    }
    else
    {
      $folder_id = $query->row('id');
    }

    $default_opts = array(
     'with_content' => true
    );
    $opts = array_merge( $default_opts , $opts );

    return $this->getFolder( $folder_id , $opts );
  }


  /*
   * Return a single folder object, and optionally its immediate children
   */
  public function getFolder( $id , $opts = array() )
  {
    $default_opts = array(
      'with_content' => false,
      'exclude_deleted' => true,
      'exclude_hidden' => false
    );
    $opts = (object)array_merge( $default_opts , $opts );

    $where_clause = array('f.id' => $id );

    if( $opts->exclude_deleted && $opts->exclude_hidden )
      $where_clause[ 'f.status' ] = FILE_STATUS_OK;
    elseif( $opts->exclude_deleted )
      $where_clause[ 'f.status !=' ] = FILE_STATUS_DELETED;
    elseif( $opts->exclude_hidden )
      $where_clause[ 'f.status !=' ] = FILE_STATUS_HIDDEN;

    $this->db->select('f.*,p.folder_name AS parent_name,p.parent_id AS grandparent_id');
    $this->db->from( 'folders f');
    $this->db->where( $where_clause );
    $this->db->join( 'folders p' , 'p.id = f.parent_id', 'left' );
    $query = $this->db->get();

    if( $query->num_rows() == 0 )
      return false;

    $folder = $query->row();
    $this->_nicePropsFolder( $folder );

    // Get the immediate children of this folder
    if( $opts->with_content )
      $folder->children = $this->getFolderChildren( $id , (array)$opts );

    return $folder;
  }

  public function getFolderTree( $root_id )
  {
    return $this->_getFolderTree( $root_id );
  }

  private function _getFolderTree( $root_id )
  {
    // create a flat list of all folders
    $query = $this->db->get_where( $this->_folder_table , array('id' => (int)$root_id , 'status !=' => FILE_STATUS_DELETED ) );
    if($query->num_rows() == 0)
      return false;
    $root_folder = $query->row();

    $flat_folders = array();
    $iterator = new ArrayIterator( array( $root_id => $root_folder ) );

    foreach( $iterator as $folder )
    {
      $folder_id = $folder->id;
      $query = $this->db->get_where( $this->_folder_table , array('parent_id' => (int)$folder_id , 'status !=' => FILE_STATUS_DELETED ) );

      $flat_folders[ $folder_id ] = $folder;

      if( $query->num_rows() == 0 )
        continue;

        foreach( $query->result() as $row )
        {
          $this->_nicePropsFolder( $row );
          $iterator->append( $row );
          $flat_folders[ $row->id ] = $row;
        }


    }

    $parents = array();

    foreach( $flat_folders as $folder )
    {
      $parents[ $folder->parent_id ][ $folder->id ] = $folder;
    }

    return (object)array(
      'tree' => $this->_folderTreeBranch( $parents , $parents[ $root_id ] ),
      'flat_folders' => $flat_folders
     );
  }

  private function _folderTreeBranch( &$parents , $children )
  {
    $tree = array();
    foreach ($children as $child)
    {
      $child_id = $child->id;

      if(isset($parents[$child_id]))
        $child->children = $this->_folderTreeBranch( $parents , $parents[ $child_id ]);

      $tree[ $child_id ] = $child;
    }

    return $tree;
  }


  public function addFolder( $data , $dry_run = false )
  {
    $response = (object) array('status'=>1,'errors'=>array());

    if(empty($data['user_id']))
    {
      $response->errors['folder_name'] = 'The folder must have an owner';
      return $response;
    }

    $user_id = $data['user_id'];
    $parent_id  = empty($data['parent_id']) ? null : $data['parent_id'];

    if(empty($data['folder_name']))
    {
      $response->errors['folder_name'] = 'A folder must have a name';
      return $response;
    }

    if(empty($parent_id))
    {
      $parent_folder = $this->getUserRootFolder( $user_id );
      $parent_id = $parent_folder->id;
    }

    // check for duplicate children of this parent
    if($this->checkDuplicate( $data['folder_name'] , 'folder' , $parent_id))
    {
      $response->errors['folder_name'] = 'There is already a file or folder with that name';
      return $response;
    }

    if( $dry_run )
      return $response;

    $insert_data = array(
     'user_id' => (int) $user_id,
     'parent_id' => (int) $parent_id,
     'folder_name' => $data['folder_name'],
     'status' => FILE_STATUS_OK,
     'created' => gmdate('Y-m-d H:i:s')
    );

    $this->db->insert( $this->_folder_table , $insert_data );

    $response->status = 0;
    $response->folder_id = $this->db->insert_id();
    return $response;
  }

  public function updateFolder( $data , $dry_run = false )
  {
    $response = (object) array('status'=>1,'errors'=>array());
    $folder_id = !empty($data['folder_id']) ? $data['folder_id'] : $data['id'];

    if(empty($folder_id))
    {
      $response->errors['folder_name'] = 'The folder was not found';
      return $response;
    }

    if(empty($data['folder_name']))
    {
      $response->errors['folder_name'] = 'A folder must have a name';
      return $response;
    }

    // check for duplicate children of this parent
    if($this->checkDuplicate( $data['folder_name'] , 'folder' , $data['parent_id'], $folder_id))
    {
      $response->errors['folder_name'] = 'There is already a file or folder with that name';
      return $response;
    }

    if( $dry_run )
      return $response;

    $update_data = array(
     'parent_id' => (int) $data['parent_id'],
     'folder_name' => $data['folder_name']
    );

    $this->db->where( array('id'=>(int)$folder_id) );
    $this->db->update( $this->_folder_table , $update_data );

    $response->status = 0;
    $response->folder_id = $folder_id;
    return $response;
  }

  public function copyFile( $dest_id , $file )
  {
    $upload_path = $this->getUploadPath();
    $dest_filepath = $this->_create_filepath( basename($file->filepath) , $upload_path );

    if( copy( $file->filepath , $dest_filepath ) )
    {
      $insert_data = array(
       'parent_id' => (int)$dest_id,
       'user_id' => (int) $file->user_id,
       'filename' => $file->filename,
       'filepath' => $dest_filepath,
       'mimetype' => $file->mimetype,
       'meta' => json_encode( $file->meta ),
       'md5' => md5_file( $dest_filepath ),
       'source' => FILE_SOURCE_COPY,
       'sharing' => (int) $file->sharing,
       'status' => (int) $file->status,
       'created' => gmdate('Y-m-d H:i:s')
      );

      $this->db->insert( $this->_file_table , $insert_data );

      return $this->db->insert_id();
    }
    else
    {
      error_log("could not copy file {$file->id} | {$file->filepath} | {$dest_filepath}" );
    }
  }

  /*
   * Copy the folder without children
   */
  public function copyFolder( $dest_id , $folder )
  {
    $dest_folder = $this->getFolder( $dest_id );

    $insert_data = array(
     'parent_id' => (int)$dest_id,
     'user_id' => (int)$dest_folder->user_id,
     'client_id' => (int)$dest_folder->client_id,
     'folder_name' => $folder->folder_name,
     'sharing' => (int)$dest_folder->sharing,
     'status' => (int)$dest_folder->status,
     'created' => gmdate( 'Y-m-d H:i:s' )
    );

    $this->db->insert( $this->_folder_table , $insert_data );

    return $this->db->insert_id();
  }

  public function copyFilesFolders( $dest_id , $file_ids = array() , $folder_ids = array() )
  {
    if(!empty($file_ids))
    {
      if(!is_array($file_ids))
        $file_ids = array( $file_ids );

      $files = $this->getFiles( $file_ids );

      foreach( $files as $file )
      {
        $this->copyFile( $dest_id , $file );
      }

    }

    if(!empty($folder_ids))
    {
      if(!is_array($folder_ids))
        $folder_ids = array( $folder_ids );

      // cannot copy a folder into itself
      if( in_array( $dest_id , $folder_ids ) )
        return;

      $new_parents = array();

      foreach( $folder_ids as $folder_id )
      {
        $tree = $this->_getFolderTree( $folder_id );

        // cannot copy a folder into itself, or into one of its children
        if( in_array( $dest_id , array_keys( $tree->flat_folders ) ) )
        {
          error_log('cannot copy a folder into itself');
          return;
        }

        // first copy over the folder structure, and map old folder ids to new ones
        foreach( $tree->flat_folders as $tree_folder )
        {
          $folder_dest_id = isset( $new_parents[ $tree_folder->parent_id ] ) ? $new_parents[ $tree_folder->parent_id ] : $dest_id;
          $new_folder_id = $this->copyFolder( $folder_dest_id , $tree_folder );
          $new_parents[ $tree_folder->id ] = $new_folder_id;
        }

        // now copy over all the files
        foreach( $tree->flat_folders as $tree_folder )
        {
          $file_children = $this->getFolderChildren( $tree_folder->id , array('folders'=>false) );

          foreach( $file_children as $file )
          {
            $file_dest_id = isset( $new_parents[ $file->parent_id ] ) ? $new_parents[ $file->parent_id ] : $dest_id;
            $this->copyFile( $file_dest_id , $file );
          }
        }
      }
    }
  }

  public function moveFilesFolders( $dest_id, $file_ids = array(), $folder_ids = array() )
  {
    if(!empty($file_ids))
    {
      if(!is_array($file_ids))
        $file_ids = array( $file_ids );

      $this->db->where(array('status !='=> FILE_STATUS_DELETED ));
      $this->db->where_in('id',$file_ids);
      $this->db->update( $this->_file_table , array('parent_id' => $dest_id ) );
    }

    if(!empty($folder_ids))
    {
      if(!is_array($folder_ids))
        $folder_ids = array( $folder_ids );

      $this->db->where(array('status !='=> FILE_STATUS_DELETED ));
      $this->db->where_in('id',$folder_ids);
      $this->db->update( $this->_folder_table , array('parent_id' => $dest_id ) );
    }

    return true;
  }

  public function changeStatus( $id , $status , $type = 'file' )
  {
    $table = $type == 'file' ? $this->_file_table : $this->_folder_table;
    $this->db->where(array('id'=>(int)$id));
    $this->db->update( $table , array('status' => (int)$status) );
  }

  public function deleteFolder( $folder_id )
  {
    // delete this folder and all child folders
    $children = $this->_getFolderTree( $folder_id );
    $folder_ids = array_keys( $children->flat_folders );

    $this->db->where_in( 'id' , $folder_ids );
    $this->db->update( $this->_folder_table , array('status' => FILE_STATUS_DELETED ) );

    // delete all child files of those folders
    $this->db->where_in( 'parent_id' , $folder_ids );
    $this->db->update( $this->_file_table , array('status' => FILE_STATUS_DELETED ) );

    return true;
  }

  /*
   * When a file is added from the Ink File Picker, the picker returns a file object
   * in javascript which includes a private URL for the converted file. Download the file
   * and add it to the system as if it were a "normal" user uploaded file
   */
  public function addFileFromInk( $user_id , $data )
  {
    // something went very wrong
    if(empty($data->filename) || empty($data->parent_id))
      return false;

    // kinda seems like this filename doesn't include an extension, guess at one
    if(!preg_match('#^.+\.[a-zA-Z0-9]{3,4}$#',$data->filename))
    {
      if( ENVIRONMENT == 'development' )
        error_log('guessing file extension from mimetype: '.$data->mimetype);
      $extension = $this->getExtensionFromMimetype( $data->mimetype );

      if($extension)
        $data->filename .= ".{$extension}";
    }

    $upload_path = $this->getUploadPath();
    $filepath = $this->_create_filepath( $data->filename , $upload_path );

    $insert_data = array(
      'user_id' => (int)$user_id,
      'parent_id' => (int)$data->parent_id,
      'filename' => $data->filename,
      'filepath' => $filepath,
      'mimetype' => $data->mimetype,
      'source' => FILE_SOURCE_INK,
      'status' => FILE_STATUS_TMP,
      'created' => gmdate('Y-m-d H:i:s')
    );

    $this->db->insert( $this->_file_table , $insert_data );
    $file = $this->getFile( $this->db->insert_id() );

    // Keep it simple for now, just fork a curl call to get the file, then
    // update the file record once curl is done

    // get the file
    $server_name = SERVER_NAME;
    $safe_filepath = escapeshellarg( $filepath );
    $safe_url = escapeshellarg( $data->url );
    $download_cmd = "curl -q --silent --create-dirs --fail --limit-rate 1M --max-time 300";
    $download_cmd .= " -o {$safe_filepath} {$safe_url} > /dev/null 2>&1";

    // update the files table
    $update_cmd = "php -f "._cli_root."/file-update.php -- {$server_name} {$file->id} > /dev/null 2>&1";

    // delete the file from ink
    $api_key = $this->ci->config->item('ink_api_key');
    $delete_cmd = "curl -q --silent --fail -X DELETE {$safe_url}?key={$api_key}";
    $delete_cmd .= "  > /dev/null 2>&1";

    $cmd = "(nohup {$download_cmd} && nohup {$update_cmd} && nohup {$delete_cmd}) &";

    if(ENVIRONMENT == 'development')
      error_log( $cmd );

    exec( $cmd );

    return $file;
  }

  public function renameFile( $file_id , $filename )
  {
    $filename = $this->_cleanFilename( $filename );
    $this->db->where(array('id'=>$file_id));
    $this->db->update( $this->_file_table , array('filename' => $filename ) );
    return true;
  }

  public function deleteFile( $file_id )
  {
    $this->db->where(array('id'=>$file_id));
    $this->db->update( $this->_file_table , array('status' => FILE_STATUS_DELETED ) );
    return true;
  }

  public function finishInkFile( $file_id = null )
  {
    if( ENVIRONMENT == 'development' )
      error_log('finishing ink file pull for ' .$file_id );

    if(empty($file_id))
      return 1;

    $file = $this->getFile( $file_id );

    if(empty($file))
      return 2;

    if($file->status != FILE_STATUS_TMP )
      return 3;

    if(!is_file($file->filepath))
      return 4;

    $this->ci->db->where(array('id'=>$file->id));
    $this->ci->db->update( $this->_file_table , array( 'status' => FILE_STATUS_OK , 'md5' => md5_file( $file->filepath ) ) );

    $this->storeMeta( $file );

    return 0;
  }

  public function storeMeta( $file )
  {
    if(!is_object($file))
      $file = $this->getFile( $file );

    if(!is_file($file->filepath))
      return;

    switch( $file->media_group )
    {
      case 'image':
        $dims = getimagesize( $file->filepath );

        $meta = array(
          'height' => $dims[1],
          'width' => $dims[0],
        );
      break;

      case 'video':
        $movie = new ffmpeg_movie( $file->filepath, false );
        $meta = array(
          'height' => $movie->getFrameHeight(),
          'width' => $movie->getFrameWidth(),
          'audio' => $movie->hasAudio(),
          'video' => $movie->hasVideo(),
          'video_codec' => null,
          'audio_codec' => null
        );

        if( $meta['video'] )
          $meta['video_codec'] = $movie->getVideoCodec();

        if( $meta['audio'] )
          $meta['audio_codec'] = $movie->getAudioCodec();
      break;
    }

    $this->db->where(array('id'=>(int)$file->id));
    $this->db->update( $this->_file_table , array('meta'=>json_encode((object)$meta)) );
  }

  public function checkDuplicate( $name , $type = 'file' , $parent_id , $exclude_id = null )
  {
    $where = array('parent_id' => $parent_id , 'status !=' => FILE_STATUS_DELETED);

    if( $exclude_id )
      $where['id !='] = $exclude_id;

    switch( $type )
    {
      case 'file':
        $where['filename'] = $name;
        $table = $this->_file_table;
      break;

      case 'folder':
        $where['folder_name'] = $name;
        $table = $this->_folder_table;
      break;
    }

    $query = $this->db->get_where( $table , $where );

    return $query->num_rows() > 0;
  }

  public function getUploadPath( $make = true )
  {
    $path = _upload_root . '/' . date('Y' ) . '/' . date('m') . '/' . date('d');

    if( $make && !is_dir( $path ))
      mkdir( $path , 0770 , true );

    return $path;
  }
  // This is quick and dirty, used to get the extension for a file when uploader does not return one
  public function getExtensionFromMimetype( $mimetype )
  {
    global $mimes;
    // use existing CI code to load the mimes array
    get_mime_by_extension('hi-there');

    foreach( $mimes as $ext => $types )
    {
      if( is_array( $types ) )
      {
        foreach( $types as $type )
        {
          if( $mimetype == $type )
            return $ext;
        }
      }

      if( $mimetype == $types )
        return $ext;
    }
  }

  private function _nicePropsFile( &$file )
  {
    $file->created = date('F j g:ia' , strtotime($file->created) );
    $file->name =& $file->filename;
    $file->ext = '';
    $file->display_name = $file->filename;
    $file->url = '/file/preview/'.$file->id;
    $file->status_str = $file->status == FILE_STATUS_OK ? 'Hide' : 'Show';

    $ext_regex = '#^.+\.[a-zA-Z0-9]{3,4}$#';

    if(preg_match( $ext_regex , basename($file->filepath)))
    {
      $name_parts = explode('.',basename($file->filepath));
      $file->ext = array_pop( $name_parts );
    }

    $file->media_group = $this->_getMediaGroupFromExt( $file->ext );

    if(preg_match( $ext_regex , $file->display_name ) )
    {
      $name_parts = explode('.', $file->display_name );
      array_pop($name_parts);
      $file->display_name = implode('.',$name_parts);
    }

    if(!empty($file->meta))
      $file->meta = json_decode( $file->meta );
  }

  private function _nicePropsFolder( &$folder )
  {
    if(empty($folder->parent_id))
      $folder->folder_name = 'Home';

    elseif(empty($folder->grandparent_id))
      $folder->parent_name = 'Home';

    $folder->created = date('F j g:ia' , strtotime($folder->created) );
    $folder->name =& $folder->folder_name;
    $folder->display_name =& $folder->folder_name;
    $folder->status_str = $folder->status == FILE_STATUS_OK ? 'Hide' : 'Show';
  }

  private function _getMediaGroupFromExt( $ext )
  {
    static $map;

    if(empty($map))
      $map = $this->_getMediaGroupMap();

    if( in_array( $ext , $map->video ) )
      return 'video';

    if( in_array( $ext , $map->audio ) )
      return 'audio';

    if( in_array( $ext , $map->image ) )
      return 'image';

    if( in_array( $ext , $map->document ) )
      return 'document';
  }

  // Map file extensions to groups of files for icons, web previews, etc
  private function _getMediaGroupMap()
  {
    return (object)array(
      'video' => array(
       '3g2', '3gp', 'avi', 'flv', 'm2v', 'm4a', 'm4v', 'mov',
       'mp4', 'mpg', 'rm', 'rpm', 'rv', 'smi', 'wm', 'wmv'
      ),
      'audio' => array(
        'aac', 'asf', 'caf', 'mp3', 'qt', 'ra', 'ram', 'wav', 'wma'
      ),
      'document' => array(
        'txt','rtf','doc','dot','docx','odt','ott','ods','ots','xls','dotx','xlsx','ppt'
      ),
      'image' => array(
        'gif','jpg','jpeg','png'
      ),
      'code' => array(
        'java','rb','py','php','c','cpp'
      )
    );
  }

  // Yoink: https://api.drupal.org/api/drupal/core%21includes%21file.inc/function/file_create_filename/8
  private function _create_filepath( $basename , $directory )
  {
    // Strip control characters (ASCII value < 32). Though these are allowed in
    // some filesystems, not many applications handle them well.
    $basename = $this->_cleanFilename( $basename );

    // A URI or path may already have a trailing slash or look like "public://".
    if (substr($directory, -1) == '/') {
      $separator = '';
    }
    else {
      $separator = '/';
    }

    $destination = $directory . $separator . $basename;

    if (file_exists($destination)) {
      // Destination file already exists, generate an alternative.
      $pos = strrpos($basename, '.');
      if ($pos !== FALSE) {
        $name = substr($basename, 0, $pos);
        $ext = substr($basename, $pos);
      }
      else {
        $name = $basename;
        $ext = '';
      }

      $counter = 0;
      do {
        $destination = $directory . $separator . $name . '_' . $counter++ . $ext;
      } while (file_exists($destination));
    }

    return $destination;
  }

  private function _cleanFilename( $filename )
  {
    return preg_replace('/[\x00-\x1F]/u', '_', $filename);
  }
}
