## Overview

A CodeIgniter library for working with files and folders. Provides a database interface to keep track of files, create and store folder assocations and provide resoruces for delivering the files on the client side. Designed to interact with the Ink File Picker API, but could be easily adapted to work with standard REST uploads, s3 storage, or another input source.

There are no extra libraries required for working with the Ink File Picker API (except cURL), and there is an optional metadata method that uses ffmpeg for media files. You can get around this requirement by simply not storing the meta data when creating file records.
